import time
import shutil
import getpass

Usuario = getpass.getuser()
Backup_de_Descargas = "C:\\Users\\" + Usuario + "\\Downloads"
Backup_de_Documentos = "C:\\Users\\" + Usuario + "\\Documents"
Backup_de_Escritorio = "C\\Users\\" + Usuario + "\\Desktop"
Backup_de_Imagenes = "C:\\Users\\" + Usuario + "\\Pictures"
Backup_de_Musica = "C:\\Users\\" + Usuario + "\\Music"
Backup_de_Videos = "C:\\Users\\" + Usuario + "\\Videos"
Fecha = time.strftime("%d-%m-%y")
Raiz = "C:\\Users\\" + Usuario + "\\Documents\\Proyecto\\" + Fecha
Raiz_ = "C:\\"

def Respaldo():

    Respaldo_Descargas1 = shutil.make_archive(base_name=Fecha+"-Descargas", format="zip", root_dir=Backup_de_Descargas)
    print("Creando el archivo:", Respaldo_Descargas1)
    shutil.move(Raiz+"-Descargas.zip", Raiz_)
    Respaldo_Documentos1 = shutil.make_archive(base_name=Fecha+"-Documentos", format="zip", root_dir=Backup_de_Documentos)
    print("Creando el archivo:", Respaldo_Documentos1)
    shutil.move(Raiz+"-Documentos.zip", Raiz_)
    Respaldo_Escritorio1 = shutil.make_archive(base_name=Fecha+"-Escritorio", format="zip", root_dir=Backup_de_Escritorio)
    ("Creando el archivo:", Respaldo_Escritorio1)
    shutil.move(Raiz+"-Escritorio.zip", Raiz_)
    Respaldo_Imagenes1 = shutil.make_archive(base_name=Fecha+"-Imagenes", format="zip", root_dir=Backup_de_Imagenes)
    print("Creando el archivo:", Respaldo_Imagenes1)
    shutil.move(Raiz+"-Imagenes.zip", Raiz_)
    Respaldo_Musica1 = shutil.make_archive(base_name=Fecha+"-Musica", format="zip", root_dir=Backup_de_Musica)
    print("Creando el archivo:", Respaldo_Musica1)
    shutil.move(Raiz+"-Musica.zip", Raiz_)
    Respaldo_Videos1 = shutil.make_archive(base_name=Fecha+"-Videos", format="zip", root_dir=Backup_de_Videos)
    print("Creando el archivo:", Respaldo_Videos1)
    shutil.move(Raiz+"-Videos.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Archivos guardados en el Disco local: {}".format(Raiz_))


def Respaldo_Descargas():

    Respaldo_Descargas = shutil.make_archive(base_name=Fecha+"-Descargas", format="zip", root_dir=Backup_de_Descargas)
    print("Creando el archivo:", Respaldo_Descargas)
    shutil.move(Raiz+"-Descargas.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Desea mover el archivo a una unidad extena?")
    print("Si (s) , No(n)")
    dec = input()
    if dec == 's' or dec == 'S':
        print("Escriba la letra de la unidad externa")
        Raiz__ = str(input())
        shutil.move(Raiz_ + Fecha + "-Descargas.zip", Raiz__+":\\")
        print("Archivos guardados en el Unidad Externa: {}".format(Raiz__))
    print("Archivos guardados en el Disco local: {}".format(Raiz_))

def Respaldo_Documentos():

    Respaldo_Documentos = shutil.make_archive(base_name=Fecha + "-Documentos", format="zip",root_dir=Backup_de_Documentos)
    print("Creando el archivo:", Respaldo_Documentos)
    shutil.move(Raiz+"-Documentos.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Desea mover el archivo a una unidad extena?")
    print("Si (s) , No(n)")
    dec = input()
    if dec == 's' or dec == 'S':
        print("Escriba la letra de la unidad externa")
        Raiz__ = str(input())
        shutil.move(Raiz_ + Fecha + "-Documentos.zip", Raiz__+":\\")
        print("Archivos guardados en el Unidad Externa: {}".format(Raiz__))
    print("Archivos guardados en el Disco local: {}".format(Raiz_))


def Respaldo_Escritorio():

    Respaldo_Escritorio = shutil.make_archive(base_name=Fecha+"-Escritorio", format="zip", root_dir=Backup_de_Escritorio)
    print("Creando el archivo:", Respaldo_Escritorio)
    shutil.move(Raiz+"-Escritorio.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Desea mover el archivo a una unidad extena?")
    print("Si (s) , No(n)")
    dec = input()
    if dec == 's' or dec == 'S':
        print("Escriba la letra de la unidad externa")
        Raiz__ = str(input())
        shutil.move(Raiz_ + Fecha + "-Escritorio.zip", Raiz__+":\\")
        print("Archivos guardados en el Unidad Externa: {}".format(Raiz__))
    print("Archivos guardados en el Disco local: {}".format(Raiz_))

def Respaldo_Imagenes():

    Respaldo_Imagenes = shutil.make_archive(base_name=Fecha+"-Imagenes", format="zip", root_dir=Backup_de_Imagenes)
    print("Creando el archivo:", Respaldo_Imagenes)
    shutil.move(Raiz+"-Imagenes.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Desea mover el archivo a una unidad extena?")
    print("Si (s) , No(n)")
    dec = input()
    if dec == 's' or dec == 'S':
        print("Escriba la letra de la unidad externa")
        Raiz__ = str(input())
        shutil.move(Raiz_ + Fecha + "-Imagenes.zip", Raiz__+":\\")
        print("Archivos guardados en el Unidad Externa: {}".format(Raiz__))
    print("Archivos guardados en el Disco local: {}".format(Raiz_))

def Respaldo_Musica():

    Respaldo_Musica = shutil.make_archive(base_name=Fecha + "-Musica", format="zip", root_dir=Backup_de_Musica)
    print("Creando el archivo:", Respaldo_Musica)
    shutil.move(Raiz+"-Musica.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Desea mover el archivo a una unidad extena?")
    print("Si (s) , No(n)")
    dec = input()
    if dec == 's' or dec == 'S':
        print("Escriba la letra de la unidad externa")
        Raiz__ = str(input())
        shutil.move(Raiz_ + Fecha + "-Musica.zip", Raiz__+":\\")
        print("Archivos guardados en el Unidad Externa: {}".format(Raiz__))
    print("Archivos guardados en el Disco local: {}".format(Raiz_))

def Respaldo_Videos():

    Respaldo_Videos = shutil.make_archive(base_name=Fecha + "-Videos", format="zip", root_dir=Backup_de_Videos)
    print("Creando el archivo:", Respaldo_Videos)
    shutil.move(Raiz+"-Videos.zip", Raiz_)
    print("Operacion culminada con exito")
    print("Desea mover el archivo a una unidad extena?")
    print("Si (s) , No(n)")
    dec = input()
    if dec == 's' or dec == 'S':
        print("Escriba la letra de la unidad externa")
        Raiz__ = str(input())
        shutil.move(Raiz_ + Fecha + "-Videos.zip", Raiz__+":\\")
        print("Archivos guardados en el Unidad Externa: {}".format(Raiz__))
    print("Archivos guardados en el Disco local: {}".format(Raiz_))


if __name__ == '__main__':

    seguir = 's'
    while seguir != 'n' and seguir != 'N':

        print("Programa para Hacer Backups")
        print("Bienvenido {}".format(Usuario))
        print("A continuacion seleccione que carpeta desea hacerles un respaldo :")
        print("1.Descargas, 2.Documentos, 3.Escritorio")
        print("4.Imagenes, 5.Musica, 6. Videos")
        print("7.Todas las anteriores")

        desicion = int(input("Que opcion escoje: "))
        if desicion == 1 :
            Respaldo_Descargas()
        elif desicion == 2:
            Respaldo_Documentos()
        elif desicion == 3:
            Respaldo_Escritorio()
        elif desicion == 4 :
            Respaldo_Imagenes()
        elif desicion == 5:
            Respaldo_Musica()
        elif desicion == 6:
            Respaldo_Videos()
        elif desicion == 7:
            Respaldo()
        else:
            print("Introdujo un numero invalido o una letra intente denuevo")
        seguir = input("Desea Copiar otra carpeta ? Si(s) , No (n):")
